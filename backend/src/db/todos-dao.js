/**
 * This file contains functions which interact with MongoDB, via mongoose, to perform Todo-related
 * CRUD operations.
 */

// TODO Exercise Three: Implement the five functions below.

export async function createTodo(todo) {

}

export async function retrieveAllTodos() {

}

export async function retrieveTodo(id) {

}

export async function updateTodo(todo) {

}

export async function deleteTodo(id) {

}